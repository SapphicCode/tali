import importlib

# reload modules
from tali import music, about, whitelist
importlib.reload(music)
importlib.reload(about)
importlib.reload(whitelist)


def setup(liara):
    liara.add_cog(music.Music(liara))
    liara.add_cog(about.About(liara))
    liara.add_cog(whitelist.Whitelist(liara))
