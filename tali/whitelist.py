import asyncio

import discord


class Whitelist:
    def __init__(self, liara):
        self.liara = liara

        self.task = self.liara.loop.create_task(self._loop())

        self.guild_whitelist = {
            81384788765712384,   # Discord API
            313211953252139009,  # NSFW dapi
        }
        self.owner_whitelist = {
            136900814408122368,  # me
            399688588654280714,  # backup owner
            163948331956043776,  # hex
            245180629744877568,  # school friend
            80088516616269824,   # danny
            120308435639074816,  # raid
            87600987040120832,   # mei
        }

    def __unload(self):
        self.task.cancel()

    async def on_guild_join(self, guild):
        if not self.liara.ready:
            return  # let's not risk it
        await asyncio.sleep(5)
        await self._check_guild(guild)

    async def _check_guild(self, guild):
        if guild.id in self.guild_whitelist:
            return
        if guild.owner and guild.owner.id in self.owner_whitelist:
            return

        try:
            await guild.leave()
        except discord.HTTPException:
            pass

    async def _loop(self):
        await self.liara.wait_until_ready()
        while True:
            for guild in self.liara.guilds:
                await self._check_guild(guild)

            await asyncio.sleep(60)
