from inspect import cleandoc

from discord.ext import commands


class About:
    def __init__(self, liara):
        self.liara = liara

    @commands.command()
    async def about(self, ctx):
        """Gives information on the bot."""

        description = """
        Hi! I'm a private bot created by {0} ({0.id}), mainly for the purposes of providing people with music.
        
        Access is provided on a per-server and per-server-owner basis.
        """
        description = cleandoc(description).format(self.liara.owner)

        whitelist = self.liara.get_cog('Whitelist')
        if whitelist:
            whitelisted_guilds = len(whitelist.guild_whitelist)
            whitelisted_owners = len(whitelist.owner_whitelist)
            description += '\n\nCurrently, {} guilds and {} owners are whitelisted.'.format(
                whitelisted_guilds, whitelisted_owners
            )

        await ctx.send(description)
