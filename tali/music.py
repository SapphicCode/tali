import asyncio
import logging
import platform
import random
import typing
from collections import deque

import aiohttp
import discord
import pylava
from discord.ext import commands

from cogs.utils.paginator import ListPaginator
from cogs.utils.storage import RedisCollection


def _user_control_predicate(ctx):
    liara = ctx.bot

    # get the channel to target
    channel = liara.lava.get_player(ctx.guild.id).channel
    if channel is None and ctx.author.voice and ctx.author.voice.channel:
        channel = ctx.author.voice.channel
    if channel is None:
        return True  # nor bot or user is in channel, control granted

    # check regular discord permissions
    if ctx.author.permissions_in(channel).move_members or ctx.author.id in liara.owners:
        return True

    # check assumed permissions
    if ctx.author in channel.members and len(channel.members) == 2:
        return True
    if len(channel.members) == 1:
        return True

    # oh well
    return False


def _check_user_control():
    return commands.check(_user_control_predicate)


class Music:
    def __init__(self, liara):
        self.liara = liara

        logger = getattr(self.liara, 'tali_logger', logging.getLogger('tali'))
        logger.setLevel(logging.INFO)
        self.liara.tali_logger = logger
        fmt = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s: %(message)s')
        hdlr = logging.FileHandler('logs/tali.log')
        hdlr.setFormatter(fmt)
        if not logger.hasHandlers():
            logger.addHandler(hdlr)
            pylava_logger = logging.getLogger('pylava')
            pylava_logger.setLevel(logging.DEBUG)
            pylava_logger.addHandler(hdlr)
        self.logger = logger.getChild('music')

        self.connection: typing.Optional[pylava.Connection] = getattr(
            self.liara, 'lava', None
        )
        if self.connection is None:
            self.connection = pylava.Connection(
                self.liara,
                'blank',
                'ws://brooke:8050',
                'http://brooke:8051'
            )
        self.liara.lava = self.connection

        self.settings = RedisCollection(self.liara.redis, 'tali.music')
        self._http = aiohttp.ClientSession(
            loop=self.liara.loop,
            headers={
                'User-Agent': 'Tali/rolling aiohttp/{} Python/{}'.format(
                    aiohttp.__version__,
                    platform.python_version()
                )
            }
        )

        self.playing: typing.Dict[int, Track] = {}
        self.queues: typing.Dict[int, typing.Deque[Track]] = {}
        self.voteskip: typing.Dict[int, typing.Set[int]] = {}
        self.queue_lock = asyncio.Lock(loop=self.liara.loop)

        self.task = self.liara.loop.create_task(self._loop())

    def __unload(self):
        self._http.close()
        self.task.cancel()

    async def _save_queues(self):
        await self.settings.set('queues', self.queues)

    async def _load_queues(self):
        self.queues = await self.settings.get('queues', {})

    async def _loop(self):
        await self.liara.wait_until_ready()
        while True:
            async with self.queue_lock:
                await self._load_queues()
                await asyncio.gather(*[self._process_guild(x) for x in self.liara.guilds], return_exceptions=True)
                await self._save_queues()
            await self._set_game()
            await asyncio.sleep(1)

    async def _process_guild(self, guild: discord.Guild):
        await asyncio.gather(
            self._process_player(guild),
            # self._process_voteskip(guild),
            return_exceptions=True
        )

    async def _process_player(self, guild: discord.Guild):
        queue = self.queues.get(guild.id, deque())
        player = self.connection.get_player(guild.id)

        if player.stopped:
            self.playing.pop(guild.id, None)

        if not player.connected:
            return

        if not queue:
            self.queues.pop(guild.id, None)
            return

        if player.stopped:
            track: Track = queue.popleft()
            if isinstance(track, SkyjamTrack):
                data = await track.get_data(self)
            else:
                data = track.data
            self.voteskip.pop(guild.id, None)
            await player.play(data)
            self.playing[guild.id] = track

    async def _process_voteskip(self, guild: discord.Guild):
        player = self.connection.get_player(guild.id)

        if player.stopped:
            return
        if not player.connected:
            return

        channel = player.channel
        votes = self.voteskip.get(guild.id, set())

        members = list(filter(lambda x: not x.bot, channel.members))
        if not members:
            return
        members_voted = list(filter(lambda x: x.id in votes, members))
        if len(members_voted) / len(members) >= 0.5:
            await player.stop()

    async def _set_game(self):
        player_count = self.connection.stats.get('playingPlayers', 0)

        status = ''
        if player_count == 0:
            status += 'nothing'
        else:
            status += 'in {} server{}'.format(
                player_count,
                's' if player_count != 1 else ''
            )
        status += ' | t!help'

        game = discord.Game(name=status)
        try:
            if self.liara.guilds[0].me.activity != game:
                await self.liara.change_presence(activity=game)
        except AttributeError:
            pass

    async def _skyjam_query(self, uri, query=None) -> aiohttp.ClientResponse:
        if query is None:
            query = {}

        token = await self.settings.get('skyjam-token')
        headers = {
            'Authorization': token
        }

        base = 'https://api-pandentia.qcx.io/skyjam'

        return await self._http.request('GET', '/'.join([base, uri]), params=query, headers=headers)

    async def skyjam_stream(self, track_id) -> typing.Tuple[int, str]:
        """
        Return tuple indicates (status, response).
        Status type 0 indicates OK, with response being the stream URL.
        Status type 1 indicates an error occurred, with the response being the error returned by the API.
        """
        resp = await self._skyjam_query(f'stream_url/{track_id}')

        data = await resp.json()
        resp.close()

        if 'error' in data:
            return 1, data['error']

        return 0, data['response']

    async def skyjam_search(self, query) -> typing.Tuple[int, typing.Union[typing.Any, str]]:
        resp = await self._skyjam_query('search', {'q': query})

        data = await resp.json()
        resp.close()

        if 'error' in data:
            return 1, data['error']

        return 0, data

    async def skyjam_album(self, album_id) -> typing.Tuple[int, typing.Union[typing.Any, str]]:
        resp = await self._skyjam_query('album/{}'.format(album_id))

        data = await resp.json()
        resp.close()

        if 'error' in data:
            return 1, data['error']

        return 0, data

    @commands.command(aliases=['summon'])
    @commands.guild_only()
    @_check_user_control()
    async def connect(self, ctx):
        """Connects to a voice channel."""
        if not (ctx.author.voice and ctx.author.voice.channel):
            return await ctx.send('You need to join a channel first.')

        guild: discord.Guild = ctx.guild
        author: discord.Member = ctx.author
        channel: discord.VoiceChannel = author.voice.channel

        player = self.connection.get_player(guild.id)
        if player.connected:
            await player.connect(channel.id)
            await ctx.send(f'Moved to {channel}.')
        else:
            await player.connect(channel.id)
            await ctx.send(f'Connected to {channel}.')

    @commands.command(aliases=['leave'])
    @commands.guild_only()
    @_check_user_control()
    async def disconnect(self, ctx):
        """Disconnects from a voice channel."""
        guild: discord.Guild = ctx.guild

        player = self.connection.get_player(guild.id)
        if player.connected:
            await player.disconnect()
            await ctx.send('Disconnected.')
        else:
            await ctx.send('No connection to disconnect.')

    @commands.command()
    @commands.guild_only()
    async def play(self, ctx, *, search: str):
        """Plays a song."""
        if 'sjsearch:' == search[:9]:
            return await self._play_skyjam(ctx, search[9:])

        player = self.connection.get_player(ctx.guild.id)

        _search = 'search:' == search[2:9]

        if not (search.startswith('http') or _search):
            search = 'ytsearch:' + search
            _search = True  # not checked again so we have to set it explicitly

        res = await player.query(search)

        if len(res) == 0:
            return await ctx.send('Query returned no results.')

        if _search:
            res = [res[0]]

        tracks = []
        for raw in res:
            if raw['info']['isStream'] and len(res) > 1:
                continue
            tracks.append(Track(raw['track'], raw['info']['title'], raw['info']['author'], ctx.author.id))

        async with self.queue_lock:
            if ctx.guild.id not in self.queues:
                self.queues[ctx.guild.id] = deque()
            self.queues[ctx.guild.id].extend(tracks)
            await self._save_queues()

        if len(tracks) == 1:
            await ctx.send('`{}` added to the queue.'.format(tracks[0]))
        else:
            await ctx.send('{} tracks added to the queue.'.format(len(tracks)))

    # noinspection PyTypeChecker
    async def _play_skyjam(self, ctx, query: str):
        if ctx.author.id not in self.liara.owners:
            return await ctx.send('You are not authorized to play songs from Skyjam.')

        album = False
        if 'a!' == query[0:2]:
            album = True
            query = query[2:]

        status, resp = await self.skyjam_search(query)
        if status != 0:
            return await ctx.send('Skyjam encountered an error: {}'.format(resp))

        tracks = []

        if album:
            if not resp['album_hits']:
                return await ctx.send('Search response returned no albums.')
            album_id = resp['album_hits'][0]['album']['albumId']
            status, album = await self.skyjam_album(album_id)
            if status != 0:
                return await ctx.send('Error while attempting to fetch search result album.')
            for raw_track in album['tracks']:
                tracks.append(SkyjamTrack(raw_track['storeId'], raw_track['title'], raw_track['artist'], ctx.author.id))
        else:
            if not resp['song_hits']:
                return await ctx.send('Search response returned no songs.')
            track = resp['song_hits'][0]['track']
            tracks.append(SkyjamTrack(track['storeId'], track['title'], track['artist'], ctx.author.id))

        async with self.queue_lock:
            if ctx.guild.id not in self.queues:
                self.queues[ctx.guild.id] = deque()
            self.queues[ctx.guild.id].extend(tracks)
            await self._save_queues()

        if len(tracks) == 1:
            await ctx.send('`{}` added to the queue.'.format(tracks[0]))
        else:
            await ctx.send('{} tracks added to the queue.'.format(len(tracks)))

    @commands.command('playing', aliases=['np', 'song'])
    @commands.guild_only()
    async def _playing(self, ctx):
        """Returns the currently-playing track."""
        guild: discord.Guild = ctx.guild

        if guild.id not in self.playing:
            return await ctx.send('Nothing is currently playing.')

        track: Track = self.playing[guild.id]

        owner = self.liara.get_user(track.owner_id)

        await ctx.send(f'{track}, queued by {owner}')

    @commands.command()
    @commands.guild_only()
    async def queue(self, ctx):
        """Returns the server's queue."""
        if ctx.guild.id not in self.queues or not self.queues[ctx.guild.id]:
            await ctx.send('The queue is currently empty. Add a track with `{}play`!'.format(
                self.liara.command_prefix[0])
            )
            return

        tracks = []
        for track in self.queues[ctx.guild.id]:
            tracks.append('{track}, queued by {owner}'.format(
                track=track,
                owner=f'<@!{track.owner_id}>'
            ))

        paginator = ListPaginator(ctx, tracks)
        await paginator.begin()

    @commands.command()
    @commands.guild_only()
    @_check_user_control()
    async def clear(self, ctx):
        """Clears the queue."""
        async with self.queue_lock:
            self.queues.pop(ctx.guild.id, None)
            await self._save_queues()
        await ctx.send('Queue cleared.')

    @commands.command()
    @commands.guild_only()
    async def skip(self, ctx):
        """Votes to skip the current track."""
        key = ctx.guild.id
        subkey = ctx.author.id

        votes = self.voteskip.get(key, set())
        if subkey not in votes:
            votes.add(subkey)
            coro = ctx.send('Skip vote added.')
        else:
            votes.remove(subkey)
            coro = ctx.send('Skip vote removed.')
        self.voteskip[key] = votes

        await coro

    @commands.command(aliases=['forceskip'])
    @commands.guild_only()
    @_check_user_control()
    async def fskip(self, ctx):
        """Forcibly skips the current track."""
        player = self.connection.get_player(ctx.guild.id)
        if player.stopped:
            return await ctx.send('Unable to skip track, nothing playing.')
        await player.stop()
        await ctx.send(f'Track forcibly skipped. (Blame {ctx.author.mention}.)')

    @commands.command()
    @commands.guild_only()
    @_check_user_control()
    async def stop(self, ctx):
        """Stops the current player."""
        async with self.queue_lock:
            self.queues.pop(ctx.guild.id, None)
            await self._save_queues()
        player = self.connection.get_player(ctx.guild.id)
        if player.stopped:
            return await ctx.send('Unable to stop player, nothing playing.')
        await player.stop()
        await ctx.send('Player stopped.')

    @commands.command()
    @commands.guild_only()
    @_check_user_control()
    async def shuffle(self, ctx):
        """Shuffles the server's queue."""
        async with self.queue_lock:
            key = ctx.guild.id
            if key in self.queues:
                random.shuffle(self.queues[key])
                await self._save_queues()
        await ctx.send('Queue shuffled.')

    @commands.command()
    @commands.guild_only()
    @_check_user_control()
    async def shuffle(self, ctx):
        """Shuffles the queue."""
        async with self.queue_lock:
            if ctx.guild.id in self.queues:
                random.shuffle(self.queues[ctx.guild.id])
                await self._save_queues()
        await ctx.send('Queue shuffled.')


class Track:
    __slots__ = ['data', 'title', 'artist', 'owner_id']

    def __init__(self, data, title, artist, owner_id):
        self.data = data
        self.title = title
        self.artist = artist
        self.owner_id = owner_id

    def __str__(self):
        return '{0.title} - {0.artist}'.format(self)

    def __repr__(self):
        return 'Track(data={0.data!r} title={0.title!r} artist={0.artist!r} owner_id={0.owner_id!r})'.format(self)


class SkyjamTrack(Track):
    __slots__ = ['track_id']

    def __init__(self, track_id, title, artist, owner_id):
        super().__init__(None, title, artist, owner_id)
        self.track_id = track_id

    async def get_data(self, music: Music):
        status, url = await music.skyjam_stream(self.track_id)
        if status != 0:
            return
        resp = await music.connection.query(url)
        return resp[0]['track']
